#!/bin/bash

# Check if reset api key
if [ "$1" == "reset" ]; then
    rm -rf ~/.config/rcommit
    echo "Api key reset"
    exit 0
fi

# check if undo
if [ "$1" == "undo" ]; then
    echo "Undoing last commit"
    git reset --soft HEAD~1
    git status
    exit 0
fi

# check if redo
if [ "$1" == "redo" ]; then
    echo "Redoing last commit"
    git commit -C HEAD@{1}
    exit 0
fi

# check if help
if [ "$1" == "help" ]; then
    echo -e "Usage: rcommit.sh [reset|undo|redo|help] [options]\n"
    echo -e "reset: Reset api key"
    echo -e "undo: Undo last commit"
    echo -e "redo: Redo last commit"
    echo -e "help: Show this help message\n"

    echo -e "Options:"
    echo -e "--dry: Print commit message without committing"
    echo -e "--enhanced: Use enhanced commit message. More context and GPT-4"
    echo -e "--yes: Commit without asking (y/n)"
    echo -e "--no-editor: Commit without editing commit message"

    echo -e "\nExample: \n  rcommit.sh --enhanced --no-editor\nrcommit will commit all staged files with an enhanced commit message without opening an editor\n"
    exit 0
fi


dry_run=false
use_enhanced=false
commit_without_asking=false
no_edit=false

all_args=("$@")
for (( i=0; i<${#all_args[@]}; i++ )); do
    if [ "${all_args[$i]}" == "--enhanced" ]; then
        use_enhanced=true
    fi

    if [ "${all_args[$i]}" == "--dry" ]; then
        dry_run=true
    fi

    if [ "${all_args[$i]}" == "--yes" ]; then
        commit_without_asking=true
    fi

    if [ "${all_args[$i]}" == "--no-editor" ]; then
        no_edit=true
    fi
done



# Read api key from ~/.config/rcommit/api_key
if [ -f ~/.config/rcommit/api ]; then
    api=$(cat ~/.config/rcommit/api)
else
    echo "No api found, enter api: "
    read api

    mkdir -p ~/.config/rcommit
    echo $api > ~/.config/rcommit/api
fi

json_escape () {
    printf '%s' "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

# check if git repo
git rev-parse --is-inside-work-tree > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Not a git repo, exiting"
    exit 1
fi

write_commit_msg() {
    msg=$1
    msg=$(echo $msg| sed 's/^"\(.*\)"$/\1/')

    if [ "$dry_run" = true ]; then
        echo -e $msg
        exit 0
    fi

    echo -e $msg > /tmp/rcommit_msg
    echo -e $msg

    commit_flag="-eF"
    if [ "$no_edit" = true ]; then
        commit_flag="-F"
    fi

    if [ "$commit_without_asking" = true ]; then
        git commit $commit_flag /tmp/rcommit_msg
        exit 0
    fi

    echo -e "\nCommit? (y/n)"
    read commit
    if [ "$commit" == "y" ]; then
        git commit $commit_flag /tmp/rcommit_msg
    fi
}

rcommit() {
    # Check if there are any diffs
    diff=$(git diff --staged)
    encoded_diff=$(json_escape "$diff")

    if [ -z "$diff" ]; then
        echo "No files added to commit, add files and try again"
        exit 1
    fi

    # Get the commit message
    msg=$(curl -X POST -H "Content-Type: application/json" -d "{\"diff\": $encoded_diff}" -s $api/commit_message)

    # check exit code
    if [ $? -ne 0 ]; then
        echo "Error getting commit message, check api key and try again"
        exit 1
    fi

    write_commit_msg "$msg"
}


rcommit_enhanced() {
    cd $(git rev-parse --show-toplevel)

    staged_files=$(git diff --staged --name-only)

    if [ -z "$staged_files" ]; then
        echo "No files added to commit, add files and try again"
        exit 1
    fi
    
    data=""

    for file in $staged_files; do
        data="$data------$file------
"
        data="$data$(git show HEAD:$file)
"
        data="$data------Diff------
"
        data="$data$(git diff --staged $file)

"
    done

    encoded_data=$(json_escape "$data")

    msg=$(curl -X POST -H "Content-Type: application/json" -d "{\"data\": $encoded_data}" -s $api/commit_message_enhanced)

    # check exit code
    if [ $? -ne 0 ]; then
        echo "Error getting commit message, check api key and try again"
        exit 1
    fi

    write_commit_msg "$msg"
}

if [ "$use_enhanced" = true ]; then
    rcommit_enhanced
else
    rcommit
fi

