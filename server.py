from fastapi import FastAPI, Request
from dotenv import load_dotenv
from fastapi.middleware.cors import CORSMiddleware
from openai import OpenAI
import os

load_dotenv()

client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"msg": "Emperor protects!"}


@app.post("/commit_message")
async def getCommitMessage(req: Request):
    result = await req.json()
    data = result["diff"]

    prompt = f"""Generate commit message for this diff:
            ```
            {data}
            ```
            """

    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        temperature=0.7,
        messages=[
            {
                "role": "system",
                "content": """You are a software developer. You are looking at a diff of code
                changes. You want to write a commit message for the diff. Write a message and rest
                of the detailed commit message in bullet points.

                Example:
                    [Bugfix] Fix bug in login page

                    Changes:
                        - Fix bug in login page
                        - Fix bug in signup page
                """,
            },
            {"role": "user", "content": prompt},
        ],
    )

    return response.choices[0].message.content


@app.post("/commit_message_enhanced")
async def getCommitMessageEnhanced(req: Request):
    result = await req.json()
    data = result["data"]

    prompt = f"""Generate commit message for this:
            ```
            {data}
            ```
            """
    
    print(prompt)
    response = client.chat.completions.create(
        model="gpt-4",
        temperature=0.7,
        messages=[
            {
                "role": "system",
                "content": """You are a software developer. You will be given source code and its
                diff. You want to write a commit message for the diff. Write a message and rest
                of the detailed commit message in bullet points.

                Example Input:
                    ------server/login.ts------
                    import { User } from "../models/user";
                    ... rest of the file
                    ------Diff------
                    @@ -1,5 +1,7 @@
                    import { User } from "../models/user";

                    ... rest of the diff
                    ----------------


                Example Output:
                    [Bugfix] Fix bug in login page

                    Changes:
                        - Fix bug in login page
                        - Fix bug in signup page
                """,
            },
            {"role": "user", "content": prompt},
        ],
    )

    return response.choices[0].message.content
