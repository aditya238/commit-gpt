#!/bin/bash

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
source ./venv/bin/activate
pip install -r requirements.txt
uvicorn server:app --port 8091 > server.log 2>&1 &
rgrok.sh https 8091 rcommit

